use serde::{Deserialize, Serialize};
use diesel::{Queryable, Insertable,AsChangeset};

#[derive(Queryable, Serialize, Deserialize,Debug,Clone,AsChangeset,Insertable)]
#[diesel(table_name=crate::models::schema::blobs)]
pub struct Blob {
    pub id: i32,
    pub data: String,
    pub value: String,
}


#[derive(Deserialize, Serialize,Debug,Clone,Insertable)]
#[diesel(table_name=crate::models::schema::blobs)]
pub struct NewBlob {
    pub data: String,
    pub value: String,
}
