// @generated automatically by Diesel CLI.

diesel::table! {
    blobs (id) {
        id -> Int4,
        data -> Text,
        value -> Text,
    }
}

diesel::allow_tables_to_appear_in_same_query!(
    blobs,
);
