use actix_web::{web, get, post, delete, HttpResponse};
use crate::{models::blob::NewBlob,repository::database::Database};

#[get("/blob")]
async fn get_blobs(db:web::Data<Database>)->HttpResponse{
    let blobs = db.get_blobs();
    match blobs {
        Ok(blob) => HttpResponse::Ok().json(blob),
        Err(err) => HttpResponse::NotFound().body(format!("Error getting all blobs: {}", err)),
    }
}

#[get("/blob/{id}")]
async fn get_blob(db:web::Data<Database>,path:web::Path<i32>)->HttpResponse{
    let blob = db.get_blob(path.into_inner());
    match blob {
        Ok(blob) =>   HttpResponse::Ok().json(blob),
        Err(err) => HttpResponse::NotFound().body(format!("Error getting blob by id: {}", err)),
    }
}

#[post("/blob")]
async fn create_blob(db:web::Data<Database>,blob:web::Json<NewBlob>)->HttpResponse{
    let blob = db.create_blob(blob.into_inner());
    match blob {
        Ok(blob)=>HttpResponse::Ok().json(blob),
        Err(err)=>HttpResponse::InternalServerError().body(format!("Error creating blob: {}", err))
    }
}

#[delete("/blob/{id}")]
async fn delete_blob(db: web::Data<Database>, path: web::Path<i32>) -> HttpResponse {
    let result = db.delete_blob(path.into_inner());
    match result {
        Ok(_) => HttpResponse::NoContent().finish(),
        Err(err) => HttpResponse::InternalServerError().body(format!("Error deleting blob: {}", err)),
    }
}

pub fn init_routes(cfg:&mut web::ServiceConfig){
    cfg.service(
        web::scope("/integration")
            .service(get_blobs)
            .service(get_blob)
            .service(create_blob)
            .service(delete_blob)
    );
}

