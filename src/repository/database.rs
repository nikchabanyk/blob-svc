use diesel::prelude::*;
use diesel::r2d2::{self, ConnectionManager};
use dotenv::dotenv;


use crate::models::blob::{Blob, NewBlob};
use crate::models::schema::blobs::dsl::*;

pub type DBPool = r2d2::Pool<ConnectionManager<PgConnection>>;


pub struct Database {
    pub pool: DBPool,
}

impl Database {
    pub fn new() -> Self {
        dotenv().ok();
        let database_url = std::env::var("DATABASE_URL")
            .expect("DATABASE_URL must be set");

        let manager = ConnectionManager::<PgConnection>::new(database_url);
        let result = r2d2::Pool::builder()
            .build(manager)
            .expect("Failed to create pool.");

        Database {
            pool: result
        }
    }

    pub fn get_blobs(&self) -> Result<Vec<Blob>, diesel::result::Error> {
        let Ok(mut connection_result) = self.pool.get() else {
            return Err(diesel::result::Error::NotFound);
        };
        let result = blobs.load::<Blob>(&mut connection_result);
        result
    }

    pub fn get_blob(&self, find_id: i32) -> Result<Blob, diesel::result::Error> {
        let Ok(mut connection_result) = self.pool.get() else{
            return Err(diesel::result::Error::NotFound)
        };
        let result = blobs
            .find(find_id)
            .first::<Blob>(&mut connection_result);
        result
    }

    pub fn create_blob(&self, blob: NewBlob) ->Result<Blob, diesel::result::Error>{
        let Ok(mut connection_result) = self.pool.get() else{
            return Err(diesel::result::Error::NotFound)
        };
        let result = diesel::insert_into(blobs)
            .values(&blob)
            .get_result(&mut connection_result);
        result
    }

    pub fn delete_blob(&self, find_id: i32) -> Result<usize, diesel::result::Error>{
        let Ok(mut connection_result) = self.pool.get() else{
            return Err(diesel::result::Error::NotFound)
        };
        let result = diesel::delete(blobs
            .filter(id.eq(find_id)))
            .execute(&mut connection_result);
        result
    }
}


