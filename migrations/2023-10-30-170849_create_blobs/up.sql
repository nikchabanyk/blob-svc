-- Your SQL goes here
CREATE TABLE blobs (
        id SERIAL PRIMARY KEY,
        data TEXT,
        value TEXT
);
