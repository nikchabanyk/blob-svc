# blob-svc

## Description

test

## Install

  ```
  git clone  https://gitlab.com/nikchabanyk/blob-svc.git
  cd blob-svc
  ```

## Running from Source

* Provide valid .env file
* Launch the service with `cargo run` command


### Database
For services, we do use ***PostgresSQL*** database.
You can [install it locally](https://www.postgresql.org/download/) or use [docker image](https://hub.docker.com/_/postgres/).
